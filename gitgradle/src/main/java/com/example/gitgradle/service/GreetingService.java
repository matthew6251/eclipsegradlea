package com.example.gitgradle.service;

public class GreetingService {
	
	public String greet(String languageIn) {

		String message = "What is that you say!";		

		if("EN".equals(languageIn)) {
			message = "Hello?!";
		}
		if("AF".equals(languageIn)) {
			message = "Hallo?!";
		}
		return message;		
	}
}
